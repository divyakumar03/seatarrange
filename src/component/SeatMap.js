import React, { useState } from 'react';
import Seat from './Seat'; 
import './Seat.css';
import './Create';
const SeatMap = () => {
    const [selectedSeats, setSelectedSeats] = useState([]); const handleSelectSeat = (seatNumber) => {
        const isSelected = selectedSeats.includes(seatNumber);
        if (isSelected) {
            setSelectedSeats(selectedSeats.filter((seat) => seat !== seatNumber));
        } else {
            if (selectedSeats.length > 0) {
                setSelectedSeats([seatNumber]);
            } else {
                setSelectedSeats([...selectedSeats, seatNumber]);
            }
        }
    }; 
    const handleConfirm = () => {
        if (selectedSeats.length === 1) {
            alert(`Selected seat: ${selectedSeats[0]}`);
        } else if (selectedSeats.length > 1) {
            setSelectedSeats([selectedSeats[selectedSeats.length - 1]]);
            alert(`Only one seat can be selected. Selected seat: ${selectedSeats[selectedSeats.length - 1]}`);
        } else {
            alert('Please select a seat.');
        }
    }; const renderSeats = () => {
        const seats = [];
        for (let row = 1; row <= 10; row++) {
            for (let col = 1; col <= 10; col++) {
                const seatNumber = `${String.fromCharCode(64 + row)}${col}`;
                const isSelected = selectedSeats.includes(seatNumber);
                seats.push(<Seat
                    key={seatNumber}
                    seatNumber={seatNumber}
                    isSelected={isSelected}
                    onSelect={handleSelectSeat}
                />
                );
            }
        }
        return seats;
    }; 
    
    return (
        
    <div className="seat-map">
        <h1 className='h1'>
            HEY,PLEASE CHOOSE A SEAT!
        </h1>
    <div className="seat-row">{renderSeats().slice(0, 10)}</div>
    <div className="seat-row">{renderSeats().slice(10, 20)}</div>
    <div className="seat-row">{renderSeats().slice(20, 30)}</div>
    <div className="seat-row">{renderSeats().slice(30, 40)}</div>
    <div className="seat-row">{renderSeats().slice(40, 50)}</div>
    <div className="seat-row">{renderSeats().slice(50, 60)}</div>
    <div className="seat-row">{renderSeats().slice(60, 70)}</div>
    <div className="seat-row">{renderSeats().slice(70, 80)}</div>
    <div className="seat-row">{renderSeats().slice(80, 90)}</div>
    <div className="seat-row">{renderSeats().slice(90, 100)}</div>
    <button className="confirm-button" onClick={handleConfirm}>
        Confirm</button>
    </div>
    );
}; 
export default SeatMap;