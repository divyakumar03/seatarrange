import React from 'react';
import './admin.css';
import { useNavigate } from "react-router-dom";

function Admin() {
    const navigate=useNavigate();
    return(
        <div className='login-page'>
            <div className='form'>
            <h3>ADMIN LOGIN</h3>
            <form className='login-form'>
                
                    <input type="text" placeholder='Enter admin id'/>
                
                
                    <input type="password" placeholder='Enter admin password'/>
                
                <button onClick={()=>navigate("/Create")} className ='button'>LOGIN</button>

            </form>
            </div>
        </div>
    )
}
export default Admin;
