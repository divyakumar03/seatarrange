
import React from 'react'; const Seat = ({ seatNumber, isSelected, onSelect }) => {
    return (
        <div
            className={`seat ${isSelected ? 'selected' : ''}`}
            onClick={() => onSelect(seatNumber)}
        >
            {seatNumber}
        </div>
    );
}; export default Seat;

