import './App.css'; 
import Admin from './component/Admin';
import Create from './component/Create'
import SeatMap from './component/SeatMap'



import { BrowserRouter, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'; 
function App() 
{
   return (
   <div className="main">

   <BrowserRouter>
   <Routes>
    <Route exact path="/" element={<Admin />} />
    <Route exact path="/Create" element={<Create />} />
    <Route exact path="/SeatMap" element={<SeatMap/>} />
   
    </Routes>
    </BrowserRouter>
    </div>
    ); 
  }
   export default App;